import React, { Component } from "react";
import CardSummaryContainer from "../components/pages/home/CardSummaryContainer";
import Calculator from "../components/pages/home/Calculator";

class Home extends Component {
  state = {};
  render() {
    return (
      <div>
        <CardSummaryContainer />
        <Calculator />
      </div>
    );
  }
}

export default Home;
