import React, { Component } from "react";
import CardSummary from "./CardSummary";

class CardSummaryContainer extends Component {
  state = {
    summary: [
      {
        id: 1,
        name: "asd",
        value: "150",
        color: "#11E1EB",
        icon: "fa-shopping-bag"
      },
      {
        id: 2,
        name: "das",
        value: "53%",
        color: "#3BB45A",
        icon: "fa-bar-chart-o"
      },
      {
        id: 3,
        name: "dszs",
        value: "44",
        color: "#FAD322",
        icon: "fa-user-plus"
      },
      {
        id: 4,
        name: "dszs",
        value: "65",
        color: "#EA470A",
        icon: "fa-pie-chart"
      }
    ]
  };

  renderCardSummary = summary => {
    return summary.map(card => <CardSummary key={card.id} card={card} />);
  };

  render() {
    const { summary } = this.state;
    return (
      <div>
        <div className="cards-summary">{this.renderCardSummary(summary)}</div>
      </div>
    );
  }
}

export default CardSummaryContainer;
