import React, { Component } from "react";
import "./App.css";
import Home from "./pages/home";

class App extends Component {
  render() {
    return (
      <div>
        <h1>Dashboard</h1>
        <Home />
      </div>
    );
  }
}

export default App;
