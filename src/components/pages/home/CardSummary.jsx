import React from "react";
import "./card-summary.css";

const cardSummary = props => (
  <div
    style={{ backgroundColor: props.card.color }}
    className="card-summary-block"
  >
    <div className="card-summary-content">
      <div className="card-summary-value">
        <h1>{props.card.value}</h1>
        <h6>{props.card.name}</h6>
      </div>
      <div className="card-summary-icon">
        <i className={"fa " + props.card.icon + " fa-5x"} />
      </div>
    </div>
    <div className="card-summary-footer">
      <h6>More Info</h6>
      <i className="fa fa-arrow-circle-right" />
    </div>
  </div>
);

export default cardSummary;
