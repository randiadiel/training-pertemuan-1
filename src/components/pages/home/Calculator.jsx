import React, { Component } from "react";

class Calculator extends Component {
  state = {
    number: {
      firstNumber: 0,
      secondNumber: 0
    },
    operator: "+"
  };

  handleChange = event => {
    const { value, name } = event.target;
    let { number } = this.state;
    number[name] = parseInt(value);
    if (Number.isNaN(number[name])) number[name] = "";
    this.setState(number);
  };

  getResult = (firstNumber, operator, secondNumber) => {
    if (Number.isNaN(firstNumber) || Number.isNaN(secondNumber)) return "";

    switch (operator) {
      case "+":
        return firstNumber + secondNumber;
      case "-":
        return firstNumber - secondNumber;
      case "*":
        return firstNumber * secondNumber;
      default:
        if (Number.isNaN(firstNumber / secondNumber)) {
          return "";
        } else return firstNumber / secondNumber;
    }
  };

  handleOperatorChange = value => {
    let { operator } = this.state;
    operator = value;
    this.setState({ operator });
  };

  render() {
    const { firstNumber, secondNumber } = this.state.number;
    const { operator } = this.state;
    return (
      <div className="m-3">
        <div>
          <input
            name="firstNumber"
            onChange={this.handleChange}
            defaultValue={firstNumber}
          />
          {operator}
          <input
            name="secondNumber"
            onChange={this.handleChange}
            defaultValue={secondNumber}
          />
          =
          <input
            value={this.getResult(firstNumber, operator, secondNumber)}
            disabled
          />
        </div>
        <button
          onClick={() => this.handleOperatorChange("+")}
          className="btn btn-success m-2"
        >
          +
        </button>
        <button
          onClick={() => this.handleOperatorChange("-")}
          className="btn btn-warning m-2"
        >
          -
        </button>
        <button
          onClick={() => this.handleOperatorChange("*")}
          className="btn btn-danger m-2"
        >
          *
        </button>
        <button
          onClick={() => this.handleOperatorChange("/")}
          className="btn btn-info m-2"
        >
          /
        </button>
      </div>
    );
  }
}

export default Calculator;
